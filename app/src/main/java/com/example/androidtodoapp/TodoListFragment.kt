package com.example.androidtodoapp

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {
    private var isDeleteMode = false
    private val adapter = RecyclerViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layout = LinearLayoutManager(activity)
        recycler_view.also {
            it.layoutManager = layout
            it.adapter = adapter
            it.addItemDecoration(DividerItemDecoration(activity, layout.orientation))

            CoroutineScope(Dispatchers.Default).launch {
                fetchTodoList()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_add_button -> {
                findNavController().navigate(R.id.fragment_todo_edit)
            }
            R.id.menu_delete_button -> {
                isDeleteMode = !isDeleteMode
                val icon =
                    if (isDeleteMode) R.drawable.ic_check else R.drawable.ic_delete
                item.setIcon(icon)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = ApiClient().apiRequest.fetchTodo().execute()
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    adapter.todoList = response.body()!!.todos!!
                }
            } else {
                withContext(Dispatchers.Main) {
                    val gson = GsonBuilder()
                        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                        .create()
                    val body =
                        gson.fromJson(response.errorBody()?.string(), BaseResponse::class.java)
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.message_unknown_error))
            }
        }
    }
}

package com.example.androidtodoapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TodoRequestBody(
    val title: String,
    val detail: String?,
    val date: Date?
) : Parcelable

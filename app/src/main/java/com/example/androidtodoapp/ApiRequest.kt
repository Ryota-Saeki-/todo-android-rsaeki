package com.example.androidtodoapp

import retrofit2.Call
import retrofit2.http.*

interface ApiRequest {
    @GET("todos")
    fun fetchTodo(): Call<TodosGetResponse>

    @POST("todos")
    fun createTodo(@Body TodoRequestBody: TodoRequestBody): Call<BaseResponse>

    @PUT("todos/{id}")
    fun updateTodo(@Path("id") id: Int, @Body TodoRequestBody: TodoRequestBody): Call<BaseResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(@Path("id") id: Int): Call<BaseResponse>
}

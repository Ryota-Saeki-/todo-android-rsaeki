package com.example.androidtodoapp

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpTextChangedListener()
        button_registration.isEnabled = false
        text_date_view.setOnClickListener {
            showDatePicker()
        }
        button_registration.setOnClickListener {
            CoroutineScope(IO).launch {
                createTodo()
            }
        }
    }

    private fun setUpTextChangedListener() {
        edit_text_title.addTextChangedListener {
            updateTitleTextCounter(it?.length ?: 0)
            updateRegistrationButton()
        }

        edit_text_detail.addTextChangedListener {
            updateDetailTextCounter(it?.length ?: 0)
            updateRegistrationButton()
        }
    }

    private fun updateTitleTextCounter(titleTextLength: Int) {
        text_title_count.text = titleTextLength.toString()
        val color = if (titleTextLength <= TITLE_MAX_CHARACTER_LIMIT) Color.BLACK else Color.RED
        text_title_count.setTextColor(color)
    }


    private fun updateDetailTextCounter(detailTextLength: Int) {
        text_detail_count.text = detailTextLength.toString()
        val color = if (detailTextLength <= DETAIL_MAX_CHARACTER_LIMIT) Color.BLACK else Color.RED
        text_detail_count.setTextColor(color)
    }

    private fun updateRegistrationButton() {
        val isValidTitleTextLength = edit_text_title.length() in 1..TITLE_MAX_CHARACTER_LIMIT
        val isValidDetailTextLength = edit_text_detail.length() <= DETAIL_MAX_CHARACTER_LIMIT
        button_registration.isEnabled = isValidTitleTextLength && isValidDetailTextLength
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            requireContext(), DatePickerDialog.OnDateSetListener { _, y, m, d ->
                text_date_view.text = ("$y/${m + 1}/$d")
            }, year, month, day
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_date_delete)
        ) { _, _ ->
            text_date_view.text = ""
        }

        datePickerDialog.show()
    }

    private fun showRegistrationSnackbar() {
        view?.also {
            Snackbar.make(it, R.string.message_registration, Snackbar.LENGTH_LONG).show()
        }
    }

    private suspend fun createTodo() {
        try {
            val response = ApiClient().apiRequest.createTodo(makeTodoRequestBody()).execute()
            if (response.isSuccessful) {
                withContext(Main) {
                    findNavController().popBackStack()
                    showRegistrationSnackbar()
                }
            } else {
                val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create()
                val body = gson.fromJson(response.errorBody()?.string(), BaseResponse::class.java)
                withContext(Main) {
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch(e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.message_unknown_error))
            }
        }
    }

    private fun makeTodoRequestBody() : TodoRequestBody {
        val format = SimpleDateFormat(FORMAT_DATE, Locale.getDefault())
        val title = edit_text_title.text.toString()
        val detail = if (edit_text_detail.text.isBlank()) null else edit_text_detail.text.toString()
        val date = if (text_date_view.text.isBlank()) null else format.parse(text_date_view.text.toString())

        return TodoRequestBody(title, detail, date)
    }

    companion object {
        private const val TITLE_MAX_CHARACTER_LIMIT = 100
        private const val DETAIL_MAX_CHARACTER_LIMIT = 1000
        private const val FORMAT_DATE = "yyyy/M/d"
    }
}

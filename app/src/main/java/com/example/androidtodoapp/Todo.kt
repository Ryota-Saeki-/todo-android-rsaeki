package com.example.androidtodoapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Todo(
    val id: Int,
    val title: String,
    val detail: String?,
    val date: Date?
) : Parcelable

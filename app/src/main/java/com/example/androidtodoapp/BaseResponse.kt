package com.example.androidtodoapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BaseResponse(
    val errorCode: Int,
    val errorMessage: String
) : Parcelable
